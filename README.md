## MilliPy 1

Apache 2.0 (C) 2021 [librehof.com](http://librehof.com)

- Minimalistic python app/service template
- Embeds [unitrepo](http://librehof.com) for easy deploy
- Development: [dev.md](doc/dev.md)
- Changelog: [changelog.md](doc/changelog.md)
- [Legal notice](NOTICE)


### Download
```
git clone --depth 10 https://gitlab.com/librehof/millipy1.git

# try demo
cd millipy1
./millipy # then open page in browser: http://127.0.0.1:8000

# update
cd millipy1
git pull [--depth n]

# remove
rm -rf millipy1
```


### Create app from

- Create new `meta` directory
  - Create file `meta/label.var` containing \<lowercase-short-name-of-your-app\>
- Copy directories: `default, media, source`
- Rename `source/millipy.py` to `source/<label>.py`
- Copy root files:
  - `millipy, .gitignore, setup, teardown, superlib, unitlib and unit`
  - Rename `millipy` to `<label>`
  - optionally copy `LICENSE`
  - optionally copy `NOTICE` and replace librehof.com
- Create `README.md`

  
### Why and When to use

- instead of just starting with a blank file
- educational: learn the anatomy of a simpel web application
- trustworthy: small enough so you can review the code in a reasonable amount of time
- take control: learn the code in a week, and adapt to your needs
- when making a service
  - having a web interface for a few admins/users
  - providing a remote api
- when making a simple desktop application
  - may serve users remotely, without remote desktop technologies
- small-footprint functionality
  - configuration
  - common time and data handling
  - log rotation
  - threading with watchdog and controlled restart/shutdown
  - web interface and remote api 
  - reduce browser reloads with simple javascript polling mechanism 
  - html, json and xml formatting
  - dev mode: changes in the web directory triggeres python and browser reload
    - valuable when doing webpage design 
- you get a web application:
  - for a LAN network (NIC=0.0.0.0) 
  - or just your local machine (NIC=127.0.0.1)<br>


### Install and Launch

- this applies to an application derived from millipy
- for development see [doc/dev.md](doc/dev.md)
- make sure you have python 3.6+ installed
- install app by cloning directly into its final location
  - under your home directory
  - or under /srv, when running as a system service on linux
- run directly in your console (see launch script below)
- or register as a service, see [unitrepo](http://librehof.com)
- launch script
  - `./<label> --help` # to print help
  - `./<label> [dev]` # run in console, optionally in dev mode
  - `./<label> pdb` # launch inside python's console debugger
- review all config created under `config/`
- optional: put your config directory under version control 
  - branch = \<host name\>
- if there is a `data/` or `rawdata/` directory: 
  - add to a backup service
- uninstall:
  - unregister service if any, see [unitrepo](http://librehof.com)
  - backup all, or `config/`, `data/` and `rawdata/`
  - delete directory
- when using `rawdata/` directory with btrfs
  - make sure to disable copy-on-write (nocow) on rawdata, before populating with files


### File structure

- root files:   
  - `<label>` - script to launch in a shell
  - `<label>.bat` - windows script to do the same (or make git-for-windows a requirement?)
- meta files:
  - `meta/label.var` - app's system name (a-z,0-9,_), accessed as **/label**
  - `meta/version.var` - 1.0dev, 1.0, 1.1.1, ..., accessed as **/version**
  - `meta/icon64.png` - app's small icon, accessed as **/favicon** (link in your html)
  - `meta/icon256.png` - app's thumbnail icon, accessed as **/icon**
- directories:
  - `meta/` - identity files (see above)
  - `[default/]` - default config files (optional)
  - `config/` - config files: app.ini, ...
  - `[data/]` - optional data directory  (linear i/o)
  - `[rawdata/]` - optional data directory with block files (random i/o)
  - `media/` - app's media files (jpg, png, ...)
  - `log/` - log files (app.log, web.log, ...)
  - `source/`
    - python and web code, see [doc/dev.md](doc/dev.md)
  
  
### Security

- there are no security features included, they need to be added
- simple security option:
  - setup VPN to reach your local network (LAN)
- advanced security option:
  - put a proxy on top of your app (like apache or nginx)
  - things you want your proxy to do:
    - answer on \<public ip\> with http**s**
    - handle user login sessions
    - handle denial-of-service attacks (give minimal time to session-less users)
    - protect against brute force attacks (n login attempts, then lock for some time)  
    - route safe http traffic to application on 127.0.0.1:80nn (8000)


### Some limitations ###

- only handles a limited number of users smoothly
- no multi-language, uses english for info/error messages
- no database framework included
