### MilliPy 1 | changelog

Apache 2.0 (C) 2023 [librehof.com](http://librehof.com) [(top)](/README.md)

2023-01-02 (v1.1)
- WebDispatcher exception fix
- genhex
- eqn instead of eq9/eq12/eq15
- jval/mval/mcontent
- ensurenofile


