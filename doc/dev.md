## MilliPy 1 | dev

Apache 2.0 (C) 2021 [librehof.com](http://librehof.com) [(top)](/README.md)

- dev.md applies to the millipy framework, and not neccesary to an application that derived from it  
- building your own framework on top? consider a name like `milli<your framework>py`


### MilliPy governance

- the owner collects and prioritizes bug reports and feature requests
- work may depend on donations
- if the owner goes underground, the repo can be forked


### Learning MilliPy by debugging

- configure `DEV=True` in `config/app.ini`
- alt 1:
  - run `./<label> pdb`
- alt 2:
  - use an IDE like pycharm, vscode, ...
  - make sure to specify `source/` and all directories under `source/` as python source directories
  - launch `source/<label>.py` from the IDE
- app debugging:
  - put a breakpoint in source/millipy.py at `main()` and continue from there
- web debugging:
  - briefly review common/buffer.py and common/hyper.py first 
  - locate and put a breakpoint in regular/webserver.py at the `doheader()` call
  - use long `WEBPOLLMS` in app.ini or disable with `None`
  - open a browser and load `http://127.0.0.1:8000`
  - now follow the complete cycle of different types of requests (highly recommended)
  - on a second reload you can learn about cache requests
  - empty browsers cache (clear data) to start over
  - use your browser's debugging features (in firefox, hit F12, goto the network tab, reload page)
  

### App concepts

- configurable object (interface):
  - `self.setup(parameters)`
  - `self.teardown()` # explicit close
  - `self.enabled()` # False if turned off by config or teared down
- `<label>.py`
  - startup and main loop (not imported by anyone)
- `state.py` (imported by app.py)
  - **APP** with global application data (meta, config and state)
- `common/` - application unaware and reuse focused
  - `common.py` imports all other common files
- `regular/` - application "regular" code, possibly resuable
- `web/` - gives user (or another app) access to the application 
  - `web.py`
    - contains url mapping used by `webdispatcher.py`
- exit mechanism:
  - `APP.SERVICE=True` if started from service manager (restart-on-failure)
  - `APP.EXITAT=<monotime>` on exit request (restart or shutdown)
  - `app.exit()` returns True if exit has been requested
  - `APP.EXITMETHOD`:
    - `"SHUTDOWN"` exit 0
    - `"RESTART"` exit 1 - restarted by launch script
    - `"ERROREXIT"` exit 2 - fatal error, restarted by launch script if service (with delay)
  - `milliping()` 
     - shall be called frequently by app-aware threads (daemon=False)
     - will raise `AppExit` exception if exit has been requested
  - the governor thread in app.py monitors app-aware threads and is the last to exit


### Persistent storage (how to implement)

- `config/` is used for config files, consider to put under separate version control (branch=hostname)
- `log/` for log files
- `local/` for locally prepared content
- enable `data/` to store data files (files with permanent state)
  - you can store "table data" in tab files, and "structured data" in json files (no query support) 
  - you can use python's sql support togehter with sqlite
  - put sqlite file in `data/<label>.sqlite` (or `config/<label>.sqlite`)
- enable `rawdata/` to store block files (data files modified using random block writes) 
- `data/` and `rawdata/` may need a backup service
- alternatively you can connect to an external database (like postgres or mysql)
  - sql model migration is not included, when needed, look at frameworks like django
  

### Coding

- develop for python 3.6 (feature wise) and avoid exotic stuff
- run on python 3.6+
- literal quotes:
  - `b'raw'` for byte strings
  - `"string"` for unicode strings
  - or `'string'` when to avoid quote escaping
  - no need to distinguish between "string", 'enum' and 'char'
  - `'''text'''` for multi-line text (tripple-quote)
- **variables** and **functions** all `lowercase`
- **classes** starts with `UpperCase`
- **globals** all `UPPERCASE`
- use f-strings exclusivly when formatting text (python 3.6)
- blank vs None:
  - blank defined as `""` for text and `b''` for raw (length 0)
  - when possible avoid values that can take both blank and None,<br>
  that is to say either allow None+filled or blank+filled
  - whenever combined, None could denote "no box" (transparent/gray),<br>
  while blank could denote "empty box" (opaque/white)
- dict:
  - dict is referred to as a "map"
  - instead of map\[key\], consider map.get(key) or map.get(key,default)
- float handling:
  - a float has maximum ~16 digits precision
  - decimal precision can be lost during calculations
  - if possible, validate your calculations to stay within expected precision  
  - a float does not carry its true decimal precision
  - true decimal precision is important when converting or comparing
  - use f-string formatting, fixed precision example: `"x={x:.2}"`
  - or use `dstr(number,decimals,dp='.',delta=False)`
  - compare using `eqd()`, `eq9()`, `eq12()`, `eq15()` or `isclose()`
- exception handling:
  - preferably use generic `Exception(message)` for plain errors to be logged
  - and use custom defined exceptions for flow-control purposes
- data categories: 
  - meta =\> static data, no change or changes with version
  - config =\> changes on restart, and possibly on request (when in some ground state)
  - state =\> changes continuously (may need locking)
  - status = aggregated state
- raw, text and string:
  - str (unicode string) is referred to as "text"
  - bytes (byte string) is referred to as "raw"
  - text without newlines is referred to as "string"
  - string, with implicit newline at the end, is referred to as "line"
- utc and local time:
  - when converting current utc or local time to a string:
    - use `hostnowdt()` or `utcnowdt()`
    - then format with datetime's strftime(format)  
  - for other cases use `utcnow()` and optionally `zonedt()`
  - utc time = unix time defined as seconds (float) since 1970-01-01 UTC
  - work in utc and convert to host's or user's local time
  - for a multi-user app: 
    - find a way to get users offset to utc
  - extra care must be taken when presenting historic time:
    - for example put in place a "user's current location" to historic utc offset
    - be explict with what timezone/offset is used to avoid confusion, and enable the user to override
    - when time is bound to a specifc location, it might of course be usefull to include the local time zone together with utc
- mono: seconds (float) since arbitrary boot time
  - use for interval measurements
  - get current mono with `mononow()`
  - get duration with `monofrom(start)`
- path can be one of:
  - dir (directory)
  - file (regular file)
  - resource (device file, socket file, ...)
- a closable (unmanaged) system object is referred to as a "handle"


### Some HTML quirks to know about

- /favicon.ico (microsoft format) loaded by default if no link tag
- javascript include: use `<script type=...></script>`, do not self-close like `<script type=.../>`
- look into csrf handling


### Some HTTP features to know about

- keep-alive (multiple requests without closing socket)
- cache control (get browsers to cache the right things, like javascript and css)
  - cache-control: must-validate
  - https://developer.mozilla.org/en-US/docs/Web/HTTP/Caching
- etags must be quoted: `"hash"`, or `W/"hash"` if hash is not based on byte content
- HTTP uses a special GMT time format (with english weekday and month names)
- chunked transfer
  - browser will never use when sending requests
  - server can use to stream api responses (like plain text and tab files)
  - not so good for content with hierarchical structure, like xml and json
- multi-part transfer
  - when you use file upload, you have to enable multi-part transfer in your form:
    ```
    <form id="upload" action="<url>" method="post" enctype="multipart/form-data">
      ...
      <input type="file">
      ...      
    </form>
    ```
- gzip compression (not yet implemented)
