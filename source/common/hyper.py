#python3.6+
#
# hyper * Apache 2.0 (C) 2021 librehof.com
#
# HTTP request and response (header encoding and decoding)
#

from basic import *

#==========================================================

# url variable string => map

# application/x-www-form-urlencoded (utf-8)

def spliturlvars(urlvarstring):

  keyvalues = urllib.parse.parse_qsl(urlvarstring)
  urlvars = { }
  for keyvalue in keyvalues:
    urlvars[keyvalue[0]] = keyvalue[1]
  return urlvars

#----------------------------------------------------------

# map => url variable string

# application/x-www-form-urlencoded (ascii)

def joinurlvars(urlvars):

  return urllib.parse.urlencode(urlvars)

#----------------------------------------------------------

# longurl => (url,urlvars)

def splitlongurl(longurl):

  split = longurl.find("?")
  if split >= 0:
    # with query vars
    url = longurl[0:split]
    urlvarstring = longurl[split+1:]
    urlvars = spliturlvars(urlvarstring)
  else:
    url = longurl
    urlvars = { }
  if url != "/":
    url = url.rstrip("/") # remove trailing /
  return (url,urlvars)

#==========================================================

# key value string => (key,value)

def hypersplitkeyvalue(kvstring, separator, blankkey=False):

  kvstring = kvstring.strip()
  split = kvstring.find(separator)
  if split >= 0:
    key = kvstring[:split].strip().lower()
    value = kvstring[split+1:].strip()
  elif blankkey:
    key = ""
    value = kvstring
  else:
    key = kvstring.lower()
    value = ""
  return (key,value)

#----------------------------------------------------------

# format utc time according to the http specification

# example: Wed, 03 Jan 2019 23:00:00 GMT

def hypergmt(utc):

  dt = Datetime.fromtimestamp(utc)
  weekday = datetoisoweekday(dt)
  weekdaystring = englishshortweekday(weekday)
  monthstring = englishshortmonth(dt.month)
  gmtstring = f"{weekdaystring}, {dt.day:02} {monthstring} "
  gmtstring += dt.strftime("%Y %H:%M:%S GMT")
  return gmtstring

#----------------------------------------------------------

# transfer-encoding => (chunked,compression) or (chunked,None)

# output converted into lower case

# input examples:
#  "identity"
#  "chunked"
#  "gzip"
#  "gzip, chunked"

def hypersplittransferencoding(transferencoding):

  transferencoding = transferencoding.strip().lower()
  values = transferencoding.split(",")
  chunked = False
  compression = None
  for value in values:
    value = value.strip()
    if value == "identity":
      pass
    elif value == "chunked":
      chunked = True
    else:
      compression = value
  return (chunked,compression)

#----------------------------------------------------------

# content-type => (mediatype,key,value) or (mediatype,None,None)

# lower case mediatype
# lower case key

# input examples:
#  "text/html; charset=UTF-8"
#  "text/plain;charset=utf-8"
#  "multipart/form-data; boundary=random"
#  "application/json" (utf-8 by default)
#  "application/xml"
#  "text/xml"
#  "image/svg+xml"

def hypersplitcontenttype(contenttype):

  contenttype = contenttype.strip()
  mediakv = contenttype.split(";")
  mediatype = mediakv[0].strip().lower()
  key = None
  value = None
  if len(mediakv) == 2:
    key,value = hypersplitkeyvalue(mediakv[1], "=")
  return (mediatype,key,value)

#----------------------------------------------------------

# header parameter line => (key,value)

# lower case key

def hypersplitheaderline(line):

  line = linewash(line)
  return hypersplitkeyvalue(line, ":")

#----------------------------------------------------------

# requestline => (method, longurl, protocol)

def hypersplitrequestline(requestline):

  requestline = linewash(requestline)
  fields = requestline.split(" ")
  if len(fields) != 3:
    raise Exception("Unknown fields in HTTP request line")
  method = fields[0].strip().upper()
  if not method:
    raise Exception("Illegal HTTP request (no method)")
  longurl = fields[1].strip()
  protocol = fields[2].strip().upper()
  return (method, longurl, protocol)

#----------------------------------------------------------

# statusline => (protocol, statuscode, statusphrase)

def hypersplitstatusline(statusline):

  statusline = linewash(statusline)
  if not statusline:
    raise Exception("Illegal HTTP response (blank status)")

  split1 = statusline.find(" ")
  if split1 < 1:
    raise Exception("Illegal HTTP response (invalid status line)")
  split2 = statusline.find(" ", split1+2) # go beyond 3-digit code
  if split2 < 0:
    raise Exception("Illegal HTTP response (invalid status phrase)")

  protocol = statusline[:split1]
  protocol = protocol.strip().upper()
  try:
    digits = statusline[split1+1:split2]
    digits = digits.strip()
    statuscode = int(digits)
  except:
    raise Exception("Illegal HTTP status code")
  statusphrase = statusline[split2+1:]
  statusphrase = statusphrase.strip()

  return (protocol, statuscode, statusphrase)

#----------------------------------------------------------

# extract name/filename from multipart header

# header lines => None or (name/None,filename/None)

def hypercontentdisposition(lines):

  disposition = None
  for line in lines:
    if not line.startswith("c") and not line.startswith("C"):
      continue
    key,disposition = hypersplitheaderline(line)
    if key == "content-disposition":
      break

  if not disposition:
    return None # not expected if multipart/form-data

  # TODO: investigate escape handling
  name = None
  filename = None
  fields = disposition.split(";")
  for field in fields:
    key,value = hypersplitkeyvalue(field, "=")
    if key == "name":
      name = value.lower().strip('"')
    elif key == "filename":
      filename = value.strip('"')

  return (name,filename)

#----------------------------------------------------------

# cookie string => map

# TODO: verify with curl.se/rfc/cookie_spec.html

def hypersplitcookiestring(cookiestring, cookies):

  kvs = cookiestring.split(";")
  for kv in kvs:
    kv = kv.strip()
    key, value = hypersplitkeyvalue(kv, "=", True)
    cookies[key] = value

#==========================================================

# http common header (shared by request and response)

class HyperCommon:

  def __init__(self):

    # header parameters (general), except those below (specific)
    self.header = { } # lower key = value

    # cookies (only expected in request)
    self.cookies = { } # lower key = value

    # set-cookies (only expected in response)
    self.setcookies = [ ] # ["cookie=value; Expires: ...", ...]

    # content-type split
    self.mediatype = "" # lower case
    self.charset = "" # lower case
    self.boundary = None # multi-part boundary
    self.rawmark = None  # multi-part b'\r\n--boundary'

    # transfer-encoding split
    self.chunked = False
    self.compression = False # lower case

    # int(content-length) or None if chunked or boundary
    self.contentlength = None

  #--------------------------------------------------------

  # returns content-type or None

  def contenttype(self):

    if self.mediatype and self.charset:
      return f"{self.mediatype}; charset={self.charset}"
    elif self.mediatype and self.boundary:
      return f"{self.mediatype}; boundary={self.boundary}"
    elif self.mediatype:
      return f"{self.mediatype}"
    else:
      return None

  #--------------------------------------------------------

  # returns transfer-encoding or None

  def transferencoding(self):

    if self.chunked and self.compression:
      return f"{self.compression}, chunked"
    elif self.compression:
      return f"{self.compression}"
    elif self.chunked:
      return f"chunked"
    else:
      return None

  #--------------------------------------------------------

  # cookie map => cookie string or None

  def cookiestring(self):

    if not self.cookies:
      return None
    kvs = []
    for key,value in self.cookies.items():
      kvs.append(f"{key}={value}")
    return "; ".join(kvs)

  #--------------------------------------------------------

  # takes header parameter "lines" and fills:
  #   header = { header key: header value, ... }
  # will break out the following parameters into specific variables:
  #   cookies = { cookie key: cookie value, ... }
  #   setcookies = [ set-cookie string, set-cookie string,  ... ]
  #   content-type => mediatype, charset, boundary
  #   content-length => contentlength
  #   transfer-encoding => chunked, compression

  def setheaderlines(self, lines):

    for line in lines:
      key,value = hypersplitheaderline(line)
      if not key:
        continue
      elif key == "content-length":
        try:
          self.contentlength = int(value)
        except:
          raise Exception("Failed to decode HTTP content-length")
      elif key == "transfer-encoding":
        self.chunked,self.compression = hypersplittransferencoding(value)
      elif key == "content-type":
        self.mediatype,key,value = hypersplitcontenttype(value)
        if key:
          if key == "boundary":
            self.setboundary(value) # as is
          elif key == "charset":
            self.charset = value.lower() # lower case
          else:
            pass # silent failure
      elif key == "set-cookie":
        self.setcookies.append(value)
      elif key == "cookie":
        hypersplitcookiestring(value, self.cookies)
      else:
        self.header[key] = value # generic

    if self.contentlength is None and not self.boundary and not self.chunked:
      self.contentlength = 0

  #--------------------------------------------------------

  # get header parameter "lines" ready for encoding

  def headerlines(self):

    if self.contentlength is None and not self.boundary and not self.chunked:
      raise Exception("HTTP header without content length information")

    lines = []

    for key,value in self.header.items():
      lines.append(f"{key}: {value}")

    cookiestring = self.cookiestring()
    if cookiestring:
      lines.append(f"cookie: {cookiestring}")

    for cookiestring in self.setcookies:
      lines.append(f"set-cookie: {cookiestring}")

    transferencoding = self.transferencoding()
    if transferencoding:
      lines.append(f"transfer-encoding: {transferencoding}")

    if self.contentlength is not None:
      lines.append(f"content-length: {self.contentlength}")

    contenttype = self.contenttype()
    if contenttype:
      lines.append(f"content-type: {contenttype}")

    return lines

  #--------------------------------------------------------

  def setboundary(self, boundary):

    self.boundary = boundary
    self.rawmark = b'\r\n--' + boundary.encode("iso-8859-1")

#==========================================================

# http request header

class HyperRequest(HyperCommon):

  def __init__(self, method=None, longurl=None):

    # callers public address
    self.address = "" # usually ipv4 like "n.n.n.n"

    # GET|POST|...
    self.method = "" # upper case

    # url (from long url, without query vars)
    self.url = "" # blank, or starts with / and ends without /

    # query vars (from long url)
    self.query = { } # key = value

    self.protocol = "HTTP/1.1" # upper case

    # dispatcher specific (set by url dispatcher)
    self.name = None # name of dynamic page or api endpoint, starts and ends without /
    self.apirev = None # requested api behaviour (integer > 0 or None if latest)

    super().__init__() # common

    if method:
      self.method = method.upper()
    if longurl:
      self.setlongurl(longurl)

  #--------------------------------------------------------

  # check if not filled (based on method)

  def empty(self):

    if self.method:
      return False
    else:
      return True

  #--------------------------------------------------------

  # split long url into url and query vars

  def setlongurl(self, longurl):

    self.url, self.query = splitlongurl(longurl)
    # set by dispatcher later on
    self.resource = ""
    self.apirev = None

    #--------------------------------------------------------

  # decode raw header (byte string) without b'\r\n\r\n'

  def decode(self, rawheader):

    if not self.empty():
      raise Exception("HTTP request already decoded")

    header = rawheader.decode("iso-8859-1")
    lines = header.split("\r\n")

    # request line
    self.method,longurl,self.protocol = hypersplitrequestline(lines[0])
    self.setlongurl(longurl)

    # header parameters
    self.setheaderlines(lines[1:])

  #--------------------------------------------------------

  # encode and return raw request header (byte string)

  def encode(self):

    if not self.method:
      raise Exception("Illegal HTTP request (blank method)")

    # url line
    header = f"{self.method} {self.url}"
    if self.query:
      header += "?"
      header += joinurlvars(self.query)
    header += f" {self.protocol}\r\n"

    # header parameters
    headerlines = self.headerlines()
    header += "\r\n".join(headerlines)

    # ending
    header += "\r\n\r\n"

    return header.encode("iso-8859-1")

  #--------------------------------------------------------

  def __str__(self):

    return f"{self.method} {self.url}"

  #--------------------------------------------------------

  def __repr__(self):

    return str(self)

#==========================================================

# http response header

class HyperResponse(HyperCommon):

  def __init__(self, statuscode=None, statusphrase=None, mediatype=None, charset=None):

    # HTTP response header

    self.protocol = "HTTP/1.1" # upper case
    self.statuscode = None
    self.statusphrase = ""

    super().__init__() # common

    if statuscode:
      self.statuscode = int(statuscode)
    if statusphrase:
      self.statusphrase = statusphrase
    if mediatype:
      self.mediatype = mediatype
    if charset:
      self.charset = charset

  #--------------------------------------------------------

  # check if not filled (based on status code)

  def empty(self):

    if self.statuscode:
      return False
    else:
      return True

  #--------------------------------------------------------

  # decode raw header without b'\r\n\r\n' (byte string)

  def decode(self, rawheader):

    if not self.empty():
      raise Exception("HTTP response already decoded")

    header = rawheader.decode("iso-8859-1")
    lines = header.split("\r\n")

    # status line
    self.protocol,self.statuscode,self.statusphrase = hypersplitstatusline(lines[0])

    # header parameters
    self.setheaderlines(lines[1:])

  #--------------------------------------------------------

  # encode and return raw response header (byte string)

  def encode(self):

    if not self.protocol:
      raise Exception("Illegal HTTP response (no protocol)")

    if not self.statuscode:
      raise Exception("Illegal HTTP response (no status)")

    # status line
    statusphrase = asciiwash(self.statusphrase)
    header = f"{self.protocol} {self.statuscode} {statusphrase}\r\n"

    # header parameters
    headerlines = self.headerlines()
    header += "\r\n".join(headerlines)

    # ending
    header += "\r\n\r\n"

    return header.encode("iso-8859-1")

  #--------------------------------------------------------

  def setstatus(self, code, phrase):

    self.statuscode = code
    self.statusphrase = phrase

  #--------------------------------------------------------

  # request key=value cookie to be saved and returned by browser

  # optional arguments:
  #  samesite="Strict"|"Lax"|"None" (defaults to Lax)
  #  maxage=<int seconds> (defaults to end-of-session, has precedence over expires, 0=delete)
  #  expires=<utc float seconds> (defaults to end-of-session)
  #  domain="<defaults to host of current URL, not including subdomain>"
  #  path="<path that must exist in URL>"
  #  secure=<boolean> (True requires https)
  #  httponly=<boolean> (True forbids JavaScript from accessing the cookie)

  # see https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Set-Cookie

  def setcookie(self, key, value, **args):

    cookiestring = f"{key}={value}"

    samesite = args.get("samesite", "Lax")
    maxage = args.get("maxage", None)
    expires = args.get("expires", None)
    domain = args.get("domain", None)
    path = args.get("path", None)
    httponly = args.get("httponly", None)
    if samesite == "None":
      Secure = True
    else:
      secure = args.get("secure", None)

    cookiestring += f"; SameSite={samesite}"
    if maxage is not None:
      cookiestring += f"; Max-Age={int(maxage)}"
    elif expires is not None:
      expiresgmt = hypergmt(expires)
      cookiestring += f"; Expires={expiresgmt}"
    if domain is not None:
      cookiestring += f"; Domain={domain}"
    if path is not None:
      cookiestring += f"; Path={path}"
    if httponly:
      cookiestring += f"; HttpOnly"
    if secure:
      cookiestring += f"; Secure"

    self.setcookies.append(cookiestring)

  #--------------------------------------------------------

  def __str__(self):

    return f"{self.statuscode} {self.statusphrase}"

  #--------------------------------------------------------

  def __repr__(self):

    return str(self)

#==========================================================

# HTTP generic exception

class HyperException(Exception):

  def __init__(self, statuscode, statusphrase, message=""):

    self.statuscode = statuscode
    self.statusphrase = statusphrase
    super().__init__(message)

#----------------------------------------------------------

# 404 Not Found

class HyperNotFound(HyperException):

  def __init__(self, url):

    super().__init__(404, "Not Found", url)

#----------------------------------------------------------

# 304 Not Modified

class HyperNotModified(HyperException):

  def __init__(self, url):

    super().__init__(304, "Not Modified", url)

#==========================================================

# HTTP generic redirect exception

class HyperRedirect(HyperException):

  def __init__(self, url, urlvars, statuscode, statusphrase):

    if urlvars:
      urlvarstring = joinurlvars(urlvars)
      longurl = f"{url}?{urlvarstring}"
    else:
      longurl = url

    super().__init__(statuscode, statusphrase, longurl)

#----------------------------------------------------------

# 301 Moved Permanently

class HyperPermanentRedirect(HyperRedirect):

  def __init__(self, url, urlvars=None):

    super().__init__(url, urlvars, 301, "Moved Permanently")

#----------------------------------------------------------

# 303 See Other (POST to GET redirect)

class HyperGetRedirect(HyperRedirect):

  def __init__(self, url, urlvars=None):

    super().__init__(url, urlvars, 303, "See Other")

#----------------------------------------------------------

# 307 Temporary Redirect

class HyperTemporaryRedirect(HyperRedirect):

  def __init__(self, url, urlvars=None):

    super().__init__(url, urlvars, 307, "Temporary Redirect")

#==========================================================
