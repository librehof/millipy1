#python3.6+
#
# shell * Apache 2.0 (C) 2021 librehof.com
#
# Run shell commands
#
# TODO:
# import shlex
# shlex.split(args)
#
# TODO: force UTF-8 or UTF-16 on windows if possible
# import ctypes
# kernel32 = ctypes.WinDLL('kernel32', use_last_error=True)
# if not kernel32.SetConsoleCP(1251):
#   raise ctypes.WinError(ctypes.get_last_error())
# # Set the active output codepage to UTF-8
# if not kernel32.SetConsoleOutputCP(65001):
#   raise ctypes.WinError(ctypes.get_last_error())
# >>> os.device_encoding(0)
# >>> os.device_encoding(1)
#

from basic import *

#==========================================================

# escaped cmdline => unescaped cmdarray

# TODO: simplify and investigate escaping rules

def cmdsplit(cmdline):

  if not isinstance(cmdline,str):
    typename = type(cmdline).__name__
    raise Exception(f"Expected command string, got {typename}")

  cmdline = linewash(cmdline, "?", " ", " ")

  cmdarray = []
  quoted = True
  escape = False
  start = None
  index = 0
  length = len(cmdline)
  while index < length:
    ch = cmdline[index]
    if not start:
      # outside
      if ch == '"':
        quoted = True
        start = index + 1
      elif ch != ' ':
        start = index
    else:
      # inside
      if quoted:
        if ch == '\\':
          escape = True
        elif escape:
          escape = False
        elif ch == '"':
          field = cmdline[start:index]
          field = field.replace('\\"', '"')
          cmdarray.append(field) # add quoted field
          start = None
      else:
        field = cmdline[start:index+1]
        field = field.replace('\\"', '"')
        cmdarray.append(field) # add field
        start = None
    index += 1

  if start:
    field = cmdline[start:]
    cmdarray.append(field) # add field
    start = None

  return cmdarray

#-----------------------------------------------------------

# unescaped cmdarray => escaped cmdline

def cmdjoin(cmdarray):

  if isinstance(cmdarray,str):
    raise Exception(f"Expected command array, got command string")

  finalarray = []
  for field in cmdarray:
    if '"' in field:
      field.replace('"', '\\"')
      field = f'"{field}"'
      finalarray.append(field)
    elif ' ' in field:
      field = f'"{field}"'
      finalarray.append(field)
    else:
      finalarray.append(field)

  return " ".join(finalarray)

#-----------------------------------------------------------

# run shell command and return stdout or exception
# cmd is either a string or an array with command and arguments
# default encoding = stdout encoding
# TODO: timeout?

def runcmd(cmd, errmin=1, place=None, pos=None, encoding=None):

  if isinstance(cmd,str):
    cmdarray = cmdsplit(cmd)
  else:
    cmdarray = cmd

  if not cmdarray:
    raise Exception("No command")

  location = cmdarray[0]
  program = pathleaf(location)
  cmdline = cmdjoin(cmdarray) # TODO: why can't we use an unescaped array

  if not encoding:
    encoding = sys.stdout.encoding

  result = subprocess.run(cmdline, capture_output=True, shell=True, check=False)

  code = result.returncode
  outstream = rawtotext(result.stdout, encoding)
  errstream = rawtotext(result.stderr, encoding)

  if code >= errmin:
    # command error
    error = None
    if errstream:
      if errstream.startswith('\n'):
        errstream = errstream[1:]
      lines = errstream.split('\n')
      error = lines[0] # first line of errstream
    if not error and outstream:
      if outstream.endswith('\n'):
        outstream = outstream[:-1]
      lines = outstream.split('\n')
      error = lines[-1]
    if not error:
      error = f"Failed with status code {code}"
    if not program in error:
      error = f"{program}: {error}"
    raise Exception(error, place, pos)

  if errstream and not outstream:
    outstream = errstream

  return outstream

#==========================================================
