#python3.6+
#
# hyper * Apache 2.0 (C) 2021 librehof.com
#
# Stream(Buffer)
#
# this generic implementation expects a file handle
# for remote communication, inherit and make non-blocking
#
# self += part   : [encode+]write part
# self.receive() : read[+decode] part and put in buffer (non-blocking)
#

from basic import *
from buffer import *

#==========================================================

# close exception (close request)

class StreamClose(Exception):

  def __init__(self, msg="Close"):

    super().__init__(msg)

#==========================================================

class Stream(Buffer):

  def __init__(self, handle, charset=None, newline=None, maxsize=None):

    self.handle = handle
    self.charset = charset
    self.newline = newline

    # buffer
    if charset:
      super().__init__("", maxsize)
    else:
      super().__init__(b'', maxsize)

  #--------------------------------------------------------

  # ensure handle is closed without error (idempotent)

  def close(self):

    self.handle = silentclose(self.handle)

  #--------------------------------------------------------

  # defines behaviour for "no data" read

  def nodata(self):

    raise StreamClose() # end-of-file

  #--------------------------------------------------------

  # low-level read (non-blocking)

  # returns: raw part or raises StreamClose()

  def read(self):

    # 128k at a time, which we consider non-blocking here,
    # as this implementation expects a file handle
    rawpart = self.handle.read(128*1024)
    if not rawpart:
      self.nodata()

  #--------------------------------------------------------

  # low-level write

  def write(self, rawpart):

    self.handle.write(rawpart)

  #--------------------------------------------------------

  # read[+decode] part and put in buffer (non-blocking)
  # returns False if no data was added

  def receive(self):

    rawpart = self.read()
    if not rawpart:
      return False

    if self.charset:
      part = rawtotext(rawpart, self.charset)
    else:
      part = rawpart

    self.push(part)
    return True

  #--------------------------------------------------------

  # [encode+]write part (+=)

  def __add__(self, part):

    if not part:
      return self

    if self.charset:
      rawpart = makeraw(part, self.charset, self.newline)
    else:
      rawpart = bytes(part)

    self.write(rawpart)
    return self

#==========================================================
