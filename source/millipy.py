#!/usr/bin/env python3
#
# Application's entry point
#

from state import * # will import initial state
from app import *
from web import *
from webserver import *

# ensure local directories
ensuredir("config")
ensuredir("log")
#ensuredir("local")
#ensuredir("data")
#ensuredir("rawdata")

#==========================================================

# setup that can not fail (exit on failure)

def setup(service, dev):

  # default app.ini
  if not pathexists("config/app.ini"):
    saveutf8("config/app.ini", appini())

  # load app.ini
  loadconfig(APP, "config/app.ini")

  APP.LOG.setup(APP.LOGFILE)

  app.note(f"{APP.LABEL} {APP.VERSION}")

  APP.SERVICE = service
  if APP.SERVICE:
    app.info("system service")

  if APP.DEV:
    app.info("dev mode")
  elif dev:
    APP.DEV = True
    app.info("dev override")

  app.threadstart("main")

  APP.WEBLOG = FileLog()
  APP.WEBLOG.setup(APP.WEBLOGFILE)

  webdispatcher = setupweb()
  webreloader = None
  if APP.DEV:
    webreloader = reloadweb
  APP.WEBSERVER = WebServer("web")
  APP.WEBSERVER.setup(APP.WEBNIC, APP.WEBPORT, webdispatcher, webreloader)

#==========================================================

def teardown():

  if APP.WEBSERVER:
    APP.WEBSERVER.teardown()

  if APP.WEBLOG:
    APP.WEBLOG.teardown()

  # we keep APP.LOG to the bitter end

  app.threadend()

#==========================================================

def run():

  # main loop
  while True:
    try:
      milliping(1) # will throw AppExit
      # do something
    except AppExit:
      return # exit
    except Exception as e:
      # error and retry
      msg = estr(e, sys.exc_info()[2], APPROOT)
      app.error(msg)
      longsleep(5)

#==========================================================

def main(args):

  try:
    service = False
    if "service" in args:
      service = True

    dev = False
    if "dev" in args:
      dev = True

    setup(service, dev)
    app.note("Running")

    # retry loop
    while True:
      try:
        milliping(10) # will throw AppExit
        run()
        break # exit
      except AppExit:
        break # exit
      except Exception as e:
        # error and retry
        msg = estr(e, sys.exc_info()[2], APPROOT)
        app.error(msg)
        longsleep(3)

  except AppExit:
    pass # we are exiting

  except Exception as e:
    # some serious error
    msg = estr(e, sys.exc_info()[2], APPROOT)
    app.error(msg)

  try:
    # save state
    pass
  except Exception as e:
    # some serious error
    msg = estr(e, sys.exc_info()[2], APPROOT)
    app.error(msg)

  try:
    teardown()
  except Exception as e:
    # some serious error
    msg = estr(e, sys.exc_info()[2], APPROOT)
    app.error(msg)

  if not APP.EXITAT or not APP.EXITMETHOD:
    # make sure we will exit
    app.requestexit("ERROREXIT", 1)

#==========================================================

if __name__ == '__main__':

  main(sys.argv[1:])

#==========================================================
