#
# Application control
#
# imported by application aware modules (from app import *)
# imports global state
#
# - timing
# - logging
# - thread/ticker control
# - shutdown/restart handling
# - shutdown request on external SIGINT/SIGTERM
# - restart request on external SIGHUP
#

from common import *

# application related python libraries
import threading
import signal
import platform

# initial global state
from state import *

# will do essential "app" setup at the bottom...

#==========================================================

# exit exception (non-error)

# raised by milliping
# request exit with app.requestexit(method)

class AppExit(Exception):

  def __init__(self):
    super().__init__("Exit")

#----------------------------------------------------------

# short real-time thread sleep

# returns time slept in milliseconds (1-1000 ms)

# time [ms]:
#  0 = switch thread (highest prio, will report 1 ms)
#  1 = fast response (high prio)
#  5, 10 or 20 = medium response
#  50, 100 or 200 = slow response
#  500 or 1000 = very slow response (low prio)

def millisleep(ms):

  if not ms:
    time.sleep(0)
    return 1
  else:
    if ms < 1:
      ms = 1
    elif ms > 1000:
      ms = 1000
    time.sleep(ms/1000)
    return ms

#----------------------------------------------------------

# short "alive" ping with shutdown/restart check

# returns time slept in milliseconds (1-1000 ms)
# will raise AppExit on shutdown/restart

# time [ms]:
#  0 = direct response (highest prio, will report 1 ms)
#  1 = fast response (high prio)
#  5, 10 or 20 = medium response
#  50, 100 or 200 = slow response
#  500 or 1000 = very slow response (low prio)

def milliping(ms):

  ms = millisleep(ms)

  current = threading.currentThread()
  if not current in APP.THREADS:
    raise Exception("Thread not registered")

  APP.THREADS[current] = APP.TICK

  if app.exit():
    raise AppExit()

  return ms

#----------------------------------------------------------

# long sleep (in seconds)

# will raise AppExit on shutdown/restart (minimum 1 second sleep)

def longsleep(sec):

  ms = int(sec) * 1000
  ms -= millisleep(1000) # 1 second guaranteed sleep
  while ms > 0:
    ms -= milliping(100) # partial interruptable sleep

#==========================================================

class app: # global methods

  # general timeout

  @staticmethod
  def timeout():

    if APP.DEV:
      return int(APP.DEVTIMEOUT)
    else:
      return int(APP.TIMEOUT)

  # - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  # request application exit

  # method: "SHUTDOWN", "RESTART", "ERROREXIT"

  @staticmethod
  def requestexit(method, delaysec=0.0):

    if APP.EXITAT:
      if delaysec == 0.0:
        APP.EXITAT = mononow()
      return

    if delaysec < 0.0:
      app.debug("exit delay is negative")
      delaysec = 0.0
    elif delaysec > 3*60:
      app.debug("exit delay too long")
      delaysec = 3*60

    msg = f"{method}"
    if delaysec > 1.5:
      msg += f" in {int(delaysec)} seconds"
    app.note(msg)

    APP.EXITMETHOD = method
    APP.EXITAT = mononow() + delaysec

    # governor thread takes over from here

    millisleep(5)

  # - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  # exit has been requested if True (called by milliping)

  @staticmethod
  def exit():

    if not APP.EXITAT:
      return False # continue
    delta = APP.EXITAT - APP.TICK
    if delta > 3*60:
      app.debug("reset delay adjusted")
      APP.EXITAT = APP.TICK + 3*60
    if delta > 0:
      return False # delay
    else:
      return True # reset!

  #--------------------------------------------------------

  # returns ticker's weakref from APP.TICKERS or None

  @staticmethod
  def findticker(ticker):

    with APP.LOCK:
      for weakticker in APP.TICKERS:
        current = weakticker() # get real ticker from weak
        if ticker == current:
          return weakticker # found

    return None # not found

  # - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  # register ticker object (light thread)

  # governor will start calling ticker.tick() periodically
  # returns tick time if added, or None if already started

  @staticmethod
  def tickerstart(ticker):

    with APP.LOCK:
      weakticker = app.findticker(ticker)
      if not weakticker:
        # add ticker
        millisleep(1)
        APP.TICK = mononow()
        weakticker = weakref.ref(ticker)
        APP.TICKERS.add(weakticker)
        return APP.TICK
      else:
        # do nothing
        return None

  # - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  # unregister ticker object (light thread)

  # ticker.tick() will not be executing/executed after return
  # returns tick time if stopped, or None if already stopped

  @staticmethod
  def tickerstop(ticker):

    with APP.LOCK:
      weakticker = app.findticker(ticker)
      if not weakticker:
        # do nothing
        return None
      try:
        millisleep(1)
        APP.TICK = mononow()
        APP.TICKERS.remove(weakticker)
        app.debug(f"ticker {id(weakticker)} stopped")
        return APP.TICK
      except:
        return None

  #--------------------------------------------------------

  # called by "app aware thread" when starting (daemon=False)

  @staticmethod
  def threadstart(label):

    if not label:
      raise Exception("Nameless thread")

    if not APP.GOVERNOR:
      # start governor and ticker on first registered thread (main thread)
      APP.GOVERNOR = threading.Thread(target=governorthread, name="governor")
      APP.GOVERNOR.start()
      ticker = threading.Thread(target=tickerthread, name="ticker")
      ticker.start()

    current = threading.currentThread()
    current.name = label

    app.debug(f"{current.name} started")

    APP.THREADS[current] = APP.TICK

  # - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  # called by "app aware thread" when ending

  def threadend():

    current = threading.currentThread()
    if current in APP.THREADS:
      del APP.THREADS[current]
      app.debug(f"{current.name} ended")

  #--------------------------------------------------------

  # print line to stdout/stderr

  printlinelock = threading.Lock()

  @staticmethod
  def printline(line, bold):

    app.printlinelock.acquire()
    try:
      if bold:
        print(line, file=sys.stderr, flush=True)
      else:
        print(line, file=sys.stdout, flush=True)
    finally:
      app.printlinelock.release()

    if bold:
      millisleep(5)
    else:
      millisleep(0)

  # - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  # helper called by global log functions

  @staticmethod
  def logline(line, bold):

    # print (skip info print if service)
    if bold or APP.DEV or not APP.SERVICE:
      app.printline(line, bold)

    # log to file
    if APP.LOG:
      APP.LOG.addline(line)

    if bold:
      millisleep(5)
    else:
      millisleep(0)

  #--------------------------------------------------------

  # debug message (dev)

  @staticmethod
  def debug(msg):

    if APP.DEV:
      msg = f" {msg}" # indent
      app.logline(msg, False)

  # - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  # info message

  @staticmethod
  def info(msg):

    app.logline(msg, False)

  # - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  # notice message (elevated)

  @staticmethod
  def note(msg):

    app.logline(msg, True)

  # - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  # warning message (elevated)

  @staticmethod
  def warning(msg):

    if not msg.startswith("WARN"):
      msg = f"WARNING: {msg}" # prepend title

    app.logline(msg, True)

  # - - - - - - - - - - - - - - - - - - - - - - - - - - - -

  # error message (elevated)

  @staticmethod
  def error(msg):

    if not msg.startswith("ERR"):
      msg = f"ERROR: {msg}" # prepend title

    app.logline(msg, True)

#==========================================================

# helper: tick all light threads once

def tickall():

  try:
    # make weakticker copy
    with APP.LOCK:
      weaktickers = list(APP.TICKERS)

    # tick all
    for weakticker in weaktickers:

      # remove if no longer active (garbage collection)
      ticker = None
      with APP.LOCK:
        APP.TICK = mononow() # update global tick
        if not weakticker in APP.TICKERS:
          continue # ticker has been removed between locks
        ticker = weakticker()
        if not ticker:
          APP.TICKERS.remove(weakticker)
          app.debug(f"ticker {id(weakticker)} stopped (garbage collected)")

      # is active?
      if ticker:
        millisleep(5) # max 200 ticks per second
        try:
          ticker.tick() # tick!
        except Exception as e:
          # ticker error
          msg = estr(e, sys.exc_info()[2], APPROOT)
          app.error(msg)

    milliping(20) # < 50 rounds per second

  except AppExit:
    pass # ignore

  except Exception as e:
    # report error
    msg = estr(e, sys.exc_info()[2], APPROOT)
    app.error(msg)
    # wait and continue
    millisleep(500)

#--------------------------------------------------------

# ticker thread

# activated after governor thread
# one last round of ticks are made after ordinary threads have ended
# second last to exit before governor thread

def tickerthread():

  app.threadstart("ticker")

  while not app.exit() or len(APP.THREADS) > 1:
    # one more round
    tickall()

  # one last round
  tickall()

  app.threadend()

#--------------------------------------------------------

# monitoring and shutdown/restart thread

# activated by first call to threadstart()
# is the last thread to exit (once activated)
# is not registered and monitored (it is the monitor)

def governorthread():

  try:
    # thread and global lock monitoring (watchdog)
    while not app.exit():
      millisleep(20)

      stall = app.timeout()
      if APP.DEV:
        stall *= 5

      # detect global lock freeze
      if APP.LOCK.acquire(timeout=stall*2):
        try:
          APP.TICK = mononow() # update global tick
        finally:
          APP.LOCK.release()
      else:
        APP.EXITMETHOD = "ERROREXIT"
        raise Exception("GLOBAL LOCK FREEZE")

      # detect thread stall
      slowest = None
      silence = 0
      for thread in list(APP.THREADS.keys()):
        try:
          tick = APP.THREADS[thread]
          period = APP.TICK - tick
          if period > silence:
            slowest = thread
            silence = period
        except:
          pass
      if silence > 10*60 and not APP.EXITAT:
        app.error(f"Thread {slowest.name} stalled for 10 minutes, restarting!")
        app.requestexit("RESTART")
      elif silence > stall:
        if not APP.STALLED:
          app.warning(f"Thread {slowest.name} stalled for {stall} seconds")
          APP.STALLED = slowest
      elif APP.STALLED:
        APP.STALLED = None

    # we are exiting
    if APP.EXITMETHOD == "SHUTDOWN":
      msg = f"Ending {APP.LABEL}"
    elif APP.EXITMETHOD == "RESTART":
      msg = f"Restarting {APP.LABEL}"
    else:
      msg = f"Exiting {APP.LABEL}"
    count = len(APP.THREADS)
    if count == 1:
      msg += f" ({count} thread)"
    elif count > 1:
      msg += f" ({count} threads)"
    app.note(msg)

    # wait for all registered threads to finish
    waitms = app.timeout() * 1000
    halfms = waitms // 2
    while waitms > 0:
      waitms -= millisleep(20)
      if not APP.THREADS:
        break # no more threads
      if waitms == halfms:
        app.note("thread is busy...")

    if not APP.THREADS:
      # now we should be the only thread left...
      if APP.EXITMETHOD == "SHUTDOWN":
        code = 0
      elif APP.EXITMETHOD == "RESTART":
        code = 1
      else:
        code = 2
      app.printline(f"exit {code}", True)
      millisleep(100)
      os._exit(code) # clean exit (!)

  except Exception as e:
    # hard error
    try: # we do not want to fail here
      msg = estr(e, sys.exc_info()[2], APPROOT)
      app.printline(msg, True)
      APP.LOG.writeline(msg)
    except:
      pass

  # hard exit, remaining threads will be destroyed
  if APP.EXITMETHOD == "SHUTDOWN":
    code = 0
  elif APP.EXITMETHOD == "RESTART":
    code = 1
  else:
    code = 2
  try:
    msg = f"HARD EXIT {code}"
    app.printline(msg, True)
    APP.LOG.writeline(msg)
    millisleep(200)
  finally:
    os._exit(code) # hard exit (!)

#==========================================================

# signal handlers

def shutdownhandler(signum, frame):

  app.requestexit("SHUTDOWN")

def restarthandler(signum, frame):

  app.requestexit("RESTART")

#--------------------------------------------------------

# essential app setup

from filelog import *
APP.LOG = FileLog()

# shutdown on external SIGINT/SIGTERM
signal.signal(signal.SIGINT, shutdownhandler)
signal.signal(signal.SIGTERM, shutdownhandler)

# restart on external SIGHUP
try:
  signal.signal(signal.SIGHUP, restarthandler)
except:
  app.note("SIGHUP not supported")

#==========================================================
