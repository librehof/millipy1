#
# Log to file, offloading writes to ticker
#
# 100 lines will be buffered before taking a loss
#

from app import *

#==========================================================

class FileLog:

  # construct (disabled)

  def __init__(self):

    self.location = None
    self.change = False

    # internal
    self.fifo = queue.SimpleQueue()
    self.lost = False # some lines have been thrown away
    self.handle = None
    self.size = 0
    self.day = ""

    # register so tick() will be called
    app.tickerstart(self)

  #--------------------------------------------------------

  # setup (enabled or disabled depending on config)

  def setup(self, location):

    if location == "":
      app.warning("Blank location (use None)")
      location = None

    if location == self.location:
      return # no change (idempotent)

    self.location = location
    self.change = True # trigger change

  #--------------------------------------------------------

  # teardown (disabled until setup)

  def teardown(self):

    self.location = None
    self.change = True # trigger close (no location)

  #--------------------------------------------------------

  # enabled or not (by config)

  def enabled(self):

    if self.location:
      return True
    else:
      return False

  #--------------------------------------------------------

  # helper called by tick()
  # or directly during emergency shutdown

  def writeline(self, line):

    if not self.handle:
      return

    # write to buffered file (utf8 with os newline)
    line += os.linesep
    raw = line.encode("utf-8")
    rawlen = len(raw)
    self.handle.write(raw)
    self.size += rawlen

  #--------------------------------------------------------

  # helper called by tick()

  def pop(self):

    try:
      return self.fifo.get(False)
    except:
      return None

  #--------------------------------------------------------

  # helper called by tick()

  def reset(self):

    if self.change:
      self.change = False
    self.handle = silentclose(self.handle)

  #--------------------------------------------------------

  # called by ticker thread

  def tick(self):

    if self.fifo.qsize() > 100:
      # overflow protection
      while self.fifo.qsize() > 90:
        self.pop()
      self.lost = True
      return

    if self.change:
      self.reset()
      return

    if not self.location:
      if self.day:
        self.day = ""
      self.reset()
      return

    try:
      flush = False

      # ensure file is open
      if not self.handle:
        self.size = 0
        self.handle = open(self.location, "ab")
        self.size = filesize(self.location)

      if self.lost:
        # mark loss of lines
        self.writeline("...") # write!
        flush = True
        self.lost = False

      # process max 10 lines
      for count in range(10):
        line = self.pop()
        if not line:
          break

        # local timestamp
        dnow = hostnowdt()
        yyyymmdd = dnow.strftime("%Y-%m-%d")
        timeofday = dnow.strftime("%H:%M:%S.%f")
        if len(timeofday) > 10:
          timeofday = timeofday[:10]
        if self.day != yyyymmdd:
          # write day line
          self.day = yyyymmdd
          dayline = f"{yyyymmdd}"
          self.writeline(dayline) # write!

        # write line
        line = line.replace("\r", "")
        line = line.replace("\n", " ")
        line = f"{timeofday}: {line}"
        self.writeline(line) # write!
        flush = True

        # rotate?
        if APP.LOGMAXKB < 1:
          maxkb = 1
        else:
          maxkb = APP.LOGMAXKB
        if self.size//1024 > maxkb:
          # rotate (!)
          app.printline(f"rotating {self.location}", True)
          self.handle.flush()
          self.handle = silentclose(self.handle)
          millisleep(5)
          copyfile(self.location, self.location+".old")
          millisleep(5)
          self.handle = open(self.location, "wb") # truncate!
          self.size = 0
          self.day = ""

      if flush:
        self.handle.flush() # flush lines

    except Exception as e:
      line = estr(e, sys.exc_info()[2], APPROOT)
      app.printline(line, True)
      self.reset()

  #--------------------------------------------------------

  def addline(self, line):

    self.fifo.put(line)

  #--------------------------------------------------------

  def __str__(self):

    return f"location({self.location})"

  #--------------------------------------------------------

  def __repr__(self):

    return str(self)

#==========================================================
