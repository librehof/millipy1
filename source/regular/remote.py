#
# Remote communication using TCP socket
#
# I/O interface (with optional text encoding):
# - Buffer with get functions
# - self.inactivity() # seconds of inactivity
# - self.receive() # receive to buffer with inactivity check
# - self += part # send
# raw i/o:
# - rawpart = self.read() # read/flush raw input (non-blocking)
# - self.write(rawpart) # write raw output
#

from app import *

#==========================================================

# Inactive for too long (close request)

class RemoteInactive(StreamClose):

  def __init__(self, inactivity):

    super().__init__(f"Inactive for {inactivity} seconds")

#==========================================================

# generic remote stream (base class)

class Remote(Stream):

  def __init__(self, handle, charset=None, newline=None, maxinactive=None, log=None):

    super().__init__(handle, charset, newline, APP.BUFMAXMB*1024*1024)

    self.address = None

    self.maxinactive = maxinactive
    self.lastactive = APP.TICK

    self.log = log

  #--------------------------------------------------------

  # returns seconds of inactivity (low precision)

  def inactivity(self):

    return int(APP.TICK - self.lastactive)

  #--------------------------------------------------------

  # defines behaviour for "nodata" read

  def nodata(self):

    if self.maxinactive:
      inactivity = self.inactivity()
      if inactivity > self.maxinactive:
        raise RemoteInactive(inactivity)
    return None

  #--------------------------------------------------------

  def __repr__(self):

    return f"address({self.address}) {super().__repr__()}"

#==========================================================
