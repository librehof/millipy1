#
# Remote communication using serial USB/UART
#

import serial # requires pip3 install pyserial

from app import *
from remote import *

#==========================================================

# serial device stream with buffer (optional text encoding)

class SerialRemote(Remote):

  def __init__(self, handle, charset=None, newline=None, maxinactive=None, log=None):

    super().__init__(handle, charset, newline, maxinactive, log)

    self.address = handle.port

    handle.timeout = 0 # make non-blocking
    handle.writeTimeout = app.timeout() # write timeout

  #--------------------------------------------------------

  # low-level read (non-blocking)

  # returns: raw part or None if no data
  # will raise RemoteInactive if maxinactive is reached

  def read(self):

    rawpart = self.handle.read(128*1024)

    # TODO: is there some way we can detect a serial disconnect other then by lack of response?

    if not rawpart:
      return self.nodata()

    if self.log:
      line = f"> {str(rawpart)}"
      self.log.addline(line)

    self.lastactive = APP.TICK
    return rawpart

  #--------------------------------------------------------

  # low-level write

  def write(self, rawpart):

    if not rawpart:
      return

    if self.log:
      line = f"< {str(rawpart)}"
      self.log.addline(line)

    self.handle.write(rawpart)
    self.lastactive = APP.TICK

#==========================================================

# returns handle to open and use with SerialRemote

def newserial(device, baud):

  handle = serial.Serial()
  handle.port = device
  handle.baudrate = baud
  #handle.bytesize = serial.EIGHTBITS
  #handle.parity = serial.PARITY_NONE
  #handle.stopbits = serial.STOPBITS_ONE
  #handle.xonxoff = False
  #handle.rtscts = False
  #handle.dsrdtr = False
  return handle

#==========================================================
