#
# WebRemote (single connection)
# and WebStream (chunked transfer)
#

from app import *
from tcpremote import *

#==========================================================

# web remote

# tcp connection with http state, one call at a time
# placeholder for Browser/REST-API single connection

class WebRemote(TcpRemote):

  def __init__(self, handle):

    super().__init__(handle)

    # server sets (keep-alive timeout in seconds or None)
    self.maxkeep = None

    # set by server when request header has been received
    # dispatcher then deals with receiving associated content
    self.req = None

    # used by dispatcher when receiving multipart/form-data header
    self.formheader = None

    # used by dispatcher when receiving chunked or multipart/form-data
    self.formdata = None # key[:filename] = rawdata|textvalue

    # set when sending response header = too late for exception response
    # dispatcher handles ordinary response
    # server handles exception response
    self.resp = None

    # dispatcher sets if delegated to thread pool
    self.delegated = False

    # dispatcher sets when completed ordinary response
    self.responded = False

    # exception put here by dispatcher, then handled by server
    # - HyperRedirect (send redirect and keep alive)
    # - StreamClose (error if active, then remove)
    # - Exception with server error (send error then remove)
    # - if self.resp then no error response can be sent (remove)
    self.exception = None
    self.message = None

  #--------------------------------------------------------

  def activecall(self):

    if self.available:
      return True
    elif self.req and not self.responded:
      return True
    else:
      return False

  #--------------------------------------------------------

  def clearcall(self):

    self.maxkeep = None
    self.req = None
    self.formheader = None
    self.formdata = None
    self.resp = None
    self.delegated = False
    self.responded = False
    self.exception = None
    self.message = None

  #--------------------------------------------------------

  def newcall(self, rawheader, maxkeep):

    req = HyperRequest()
    req.address = self.address
    req.decode(rawheader)

    self.maxkeep = maxkeep
    self.req = req
    self.formheader = None
    self.formdata = None
    self.resp = None
    self.delegated = False
    self.responded = False
    self.exception = None
    self.message = None

  #--------------------------------------------------------

  def setexception(self, e, tb=None):

    self.exception = e
    if tb:
      self.message = estr(e, tb, APPROOT)

  #--------------------------------------------------------

  # send http response header

  def sendheader(self, resp):

    if not resp.mediatype:
      if resp.charset:
        resp.mediatype = "text/plain"
      else:
        resp.mediatype = "application/octet-stream"

    if not self.maxkeep:
      # tell other side to close connection
      resp.header["connection"] = "close"
    elif not "connection" in resp.header and not "keep-alive" in resp.header:
      # tell other side to reuse connection
      resp.header["connection"] = "keep-alive"
      if APP.WEBMAXOPEN >= 20:
        clientmaxopen = 10 + APP.WEBMAXOPEN//10
      else:
        clientmaxopen = APP.WEBMAXOPEN//2
      resp.header["keep-alive"] = f"timeout={self.maxkeep}, max={clientmaxopen}"

    rawheader = resp.encode()
    self.resp = resp # no exception reporting can be done from here on
    self.write(rawheader) # write!

  #--------------------------------------------------------

  # send raw content

  def sendcontent(self, rawcontent=None):

    if rawcontent:
      self.write(rawcontent) # write!
    self.responded = True

  #--------------------------------------------------------

  # send http header + raw content (using content-length)

  def sendresponse(self, resp, rawcontent=None):

    if rawcontent:
      resp.contentlength = len(rawcontent)
    else:
      resp.contentlength = 0
    self.sendheader(resp) # write!
    if rawcontent:
      self.write(rawcontent) # write!
    self.responded = True

#==========================================================

# web stream (chunked data)

class WebStream(TcpRemote):

  def __init__(self, handle, charset=None):

    super().__init__(handle, charset)

  #--------------------------------------------------------

  def __add__(self, part):

    if not part:
      return self
    if self.charset:
      rawpart = makeraw(part, self.charset)
    else:
      rawpart = part
    length = len(rawpart)
    header = "%X\r\n" % length
    rawheader = header.encode("ascii")
    self.write(rawheader)
    self.write(rawpart)
    return self

#==========================================================
