#
# Global application static/config/data/state
#

# establish application's root and source directories
import os
import sys
import platform
import threading
os.chdir(os.path.dirname(os.path.realpath(__file__)))
APPSOURCE = os.getcwd()
os.chdir(os.path.dirname(APPSOURCE))
APPROOT = os.getcwd()
APPHOST = platform.node()
if not APPHOST:
  APPHOST = "host"
sys.path.append(f"{APPSOURCE}/common")
sys.path.append(f"{APPSOURCE}/regular")
sys.path.append(f"{APPSOURCE}/web")

from common import *

#==========================================================

# global app variables

class APP:

  # app meta

  TITLE = "MilliPy"
  LABEL = loadutf("meta/label.var").replace("\n", "")
  VERSION = loadutf("meta/version.var").replace("\n", "")

  # web meta (setupweb)

  UTFEXTS = { } # { ".ext": "textmediatype" }
  RAWEXTS = { } # { ".ext": "rawmediatype" }

  #--------------------------------------------------------

  # app config

  DEV = False # debug output, longer timeouts, ...
  SERVICE = False # started by a service manager

  TIMEOUT = 10
  DEVTIMEOUT = 40
  BUFMAXMB = 1024
  LOGMAXKB = 500

  LOGFILE = "log/app.log"

  # web config

  WEBNIC = None
  WEBPORT = 8000

  WEBPOLLMS = 300
  WEBMAXOPEN = 10
  WEBCHECKPORT = True

  WEBLOGFILE = None

  #--------------------------------------------------------

  # app state

  LOCK = threading.RLock() # global lock

  TICK = mononow() # low precision tick time (monotime)

  EXITAT = None # exit when TICK > EXITAT, see app.exit()
  EXITMETHOD = None # "SHUTDOWN" (0), "RESTART" (1), "ERROREXIT" (2)

  GOVERNOR = None # governor thread

  # app aware threads (daemon=False)
  THREADS = { } # thread = tick (updated when calling milliping)
  STALLED = None # thread not responding, with longest silence

  # light threads, governor will lock and execute ticker.tick()
  TICKERS = set() # set of weakref.ref(ticker)

  LOG = None # = FileLog()

  # web state

  WEBLOG = None # = FileLog()
  WEBSERVER = None # = WebServer()
  WEBRELOADED = False # use to reload web page (dev mode)

  #--------------------------------------------------------

  # demo data

  FIRSTNAME = ""
  FREETEXT = ""
  NOTE = ""
  FILENAME = ""
  UPLOAD = b''

#==========================================================

# default app.ini

def appini():

  #-------------------------------
  return f'''\
# {APP.LABEL} application config

# development mode (debug)
DEV=False

# general timeout and max buffer and log
#TIMEOUT={APP.TIMEOUT}
#DEVTIMEOUT={APP.DEVTIMEOUT}
#BUFMAXMB={APP.BUFMAXMB}
#LOGMAXKB={APP.LOGMAXKB}

# application log
LOGFILE="{APP.LOGFILE}"
#LOGFILE=None

WEBNIC="127.0.0.1" # listen on host's local interface
#WEBNIC="0.0.0.0" # listen on all network interfaces
#WEBNIC=None

WEBPORT=8000  # port to connect to

#WEBPOLLMS={APP.WEBPOLLMS} # browser poll time [ms]
#WEBMAXOPEN={APP.WEBMAXOPEN} # max active connections
#WEBCHECKPORT=True # check port is not in use

WEBLOGFILE=None
#WEBLOGFILE="log/web.log"
''' #-----------------------------

#==========================================================
