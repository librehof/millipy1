from web import *

#========================================================

def header(reqtype, resptype, req, resp):

  key = "api"
  if not key in req.cookies:
    value = random.randint(1000, 9999)
    resp.setcookie(key, value)
    req.cookies[key] = value

#========================================================

def now(req, nonei, mapo):

  # ignoring GET/POST (req.method)

  dnow = hostnowdt()
  if req.apirev and req.apirev <= 1:
    mapo["now"] = dnow.strftime("%Y-%m-%d %H:%M:%S")
  else:
    mapo["day"] = dnow.strftime("%Y-%m-%d")
    mapo["time"] = dnow.strftime("%H:%M:%S")

#========================================================
