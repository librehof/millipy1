from app import *

#========================================================

def header(reqtype, resptype, req, resp):

  key = "web"
  if not key in req.cookies:
    value = random.randint(1000, 9999)
    #now = utcnow()
    #end = now + (10*24*60*60)
    #resp.setcookie(key, value, expires=end)
    resp.setcookie(key, value)
    req.cookies[key] = value

  if req.url == "/download":
    resp.header["Content-Disposition"] = f'attachment; filename="{APP.FILENAME}"';
    ext = pathext(APP.FILENAME)
    if ext in APP.UTFEXTS:
      resp.mediatype = APP.UTFEXTS[ext]
    elif ext in APP.RAWEXTS:
      resp.mediatype = APP.RAWEXTS[ext]

#========================================================

def top(req, formi, texto):

  onload=""
  pollms = APP.WEBPOLLMS
  if pollms:
    if pollms < 50:
      pollms = 50
    onload=f"tick('{req.name}',{pollms});"

  # - - - - - - - - - - - - - - - - - -
  texto += f'''\
<html>
<head>
<title>{APP.TITLE}</title>
<link rel="icon" type="image/png" sizes="64x64" href="/favicon"/>
<link rel="icon" type='image/png' sizes="256x256" href="/icon"/>
<link rel="stylesheet" href="/style.css"/>
<script type="text/javascript" src="/script.js" defer></script>
</head>
<body onload="{onload}">
hello {req.address} • session {req.cookies.get("web")} • <span id="clock">clock</span><br>
<br>
'''
  # - - - - - - - - - - - - - - - - - -

#========================================================

def bottom(req, formi, texto):

  # - - - - - - - - - - - - - - - - - -
  texto += f'''\
<div id="graycover" class="graycover"></div>
</body>
</html>
'''
  # - - - - - - - - - - - - - - - - - -

#========================================================

def poll(req, mapi, jso):

  if APP.WEBRELOADED:
    # dev mode reload
    APP.WEBRELOADED = False # ack
    jso += "reloadpage();\n"

  dt = hostnowdt()
  clock = dt.strftime("%Y-%m-%d %H:%M:%S")

  # - - - - - - - - - - - - - - - - - -
  jso += f'''\
settext("clock","{clock}");
'''
  # - - - - - - - - - - - - - - - - - -

#========================================================
