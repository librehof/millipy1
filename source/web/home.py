from web import *

#========================================================

def body(req, formi, texto):

  if req.method == "POST":
    millisleep(250) # simulate delay
    if "upload" in formi:
      filename = formi.get("upload:filename")
      filename = pathwash(filename)
      APP.FILENAME = pathleaf(filename)
      APP.UPLOAD = formi.get("upload")
      APP.NOTE = formi.get("note")
    else:
      APP.FIRSTNAME = formi.get("firstname")
      APP.FREETEXT = formi.get("freetext")
    raise HyperGetRedirect(req.url)

  # - - - - - - - - - - - - - - - - - -
  texto += f'''\
<img src="icon" alt="icon" width="64"><br>
<br>
<span class="box">
<form id="home" action={mval(req.url)} method="post">
  <label for="firstname">First name:</label>
  <input type="text" id="firstname" name="firstname" value={mval(APP.FIRSTNAME)}><br><br>
  <label for="freetext">Free text:</label><br>
  <textarea id="freetext" name="freetext" rows="8" cols="80">{mcontent(APP.FREETEXT)}</textarea><br><br>
  <input onclick="apply('home');" type="button" value="Submit">
</form>
</span><br>
<br>
<span class="box">
<form id="upload" action={mval(req.url)} method="post" enctype="multipart/form-data">
  <label for="upload">File:</label>
  <input type="file" id="upload" name="upload"><br><br>
  <label for="note">Note:</label>
  <input type="text" id="note" name="note" value={mval(APP.NOTE)}><br><br>
  <input onclick="apply('upload');" type="button" value="Submit">
</form>
<br>
<a href="/download">Download</a><br>
</span><br>
<br>
<span class="box">
<a href="/stream">Stream</a><br>
</span><br>
'''
  # - - - - - - - - - - - - - - - - - -

#========================================================

# home specific poll data

def poll(req, mapi, jso):

  # - - - - - - - - - - - - - - - - - -
  jso += f'''\
'''
  # - - - - - - - - - - - - - - - - - -

#========================================================

def download(req, formi, rawo):

  rawo += APP.UPLOAD

#========================================================

def stream(req, formi, streamo):

  for i in range(1,100):
    streamo += f"{i}\tline {i}\n"
    milliping(50) # simulate delay

#========================================================
