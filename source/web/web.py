#
# Web structure
#
# TODO: update interface description
#
# Dispatcher interface:
#
# web call (resp.charset="utf-16" => utf-16le with bom)
#   <precall>(reqtype, resptype, req, resp)
#   <call>(req, <reqtype>i, <resptype>o)
#
# dynamic = {
#   url: (name, reqtype, resptype, <precall>[, <call>, ...]),
#   url: HyperRedirect(goto),
#   url: Exception(error message),
# }
#
# api = { # url = /api[rev]/<endpoint>
#   endpoint: (reqtype, resptype, <precall>[, <call>]),
#   endpoint: HyperRedirect(goto),
#   endpoint: Exception(error),
# }
#
# datatypes used for reqtype/resptype:
# - None => None / None
# - "raw" => bytes/Raw (application/octet-stream)
# - "text" => str/Text (text/plain)
# - "form:map" => dict/-
# - "html:text" => str/HtmlText (text/html)
# - "xml:text" => str/MarkupText (text/xml)
# - "js:text" => -/JsonText (text/javascript)
# - "json:text" => str/JsonText (application/json)
# - "json:map" => dict/dict (application/json)
# - "tab:list" => list/list (text/tab-separated-values)
# - ".ext:text" => str/Text (ext=>mediatype)
# - ".ext:raw" => bytes/Raw (ext=>mediatype)
# - ".ext:textstream" => -/WebStream(text) (ext=>mediatype)
# - ".ext:rawstream" => -/WebStream(raw) (ext=>mediatype)
#
# text decoding in priority order:
# - by charset
# - utf-8/16 with bom
# - utf-8 without bom
#

import importlib

from app import *
from webdispatcher import *

# dynamic
import api
import frame
import home

#==========================================================

# called by main during setup
# returns webdispatcher

def setupweb():

  # global
  APP.UTFEXTS = {
    ".txt": "text/plain",
    ".var": "text/plain",
    ".list": "text/plain",
    ".tab": "text/tab-separated-values",
    ".html": "text/html",
    ".css": "text/css",
    ".js": "text/javascript",
    ".json": "application/json",
    ".xml": "text/xml",
    ".svg": "image/svg+xml",
    ".mml": "text/mathml",
  }

  # global
  APP.RAWEXTS = {
    ".ico": "image/x-icon",
    ".png": "image/png",
    ".jpg": "image/jpeg",
    ".jpeg": "image/jpeg",
    ".mp3": "audio/mpeg",
    ".mpg": "video/mpeg",
    ".mpeg": "video/mpeg",
    ".pdf": "application/pdf",
    ".zip": "application/zip",
  }

  media = [
    ("/media", "media"), # url=/media/<file>
  ]

  constant = [
    ("/label", "meta/label.var"),
    ("/version", "meta/version.var"),
    ("/repoid", "meta/repoid.var"),
    ("/favicon", "meta/icon64.png"), #("/favicon.ico", "meta/icon64.ico"),
    ("/icon", "meta/icon256.png"),
    ("/script.js", f"{APPSOURCE}/web/script.js"),
    ("/style.css", f"{APPSOURCE}/web/style.css"),
  ]

  dynamic = {
    "/index.html": HyperPermanentRedirect("/"),
    "/home": HyperPermanentRedirect("/"),
    "/": ("home", "form:map", "html:text", frame.header, frame.top, home.body, frame.bottom),
    "/homepoll": ("home", "json:map", "js:text", frame.header, frame.poll, home.poll),
    "/download": ("download", "form:map", "raw", frame.header, home.download),
    "/stream": ("stream", "form:map", ".txt:textstream", frame.header, home.stream),
  }

  endpoints = { # url = /api<rev>/<[subapi/]endpoint>
    "now": (None, "json:map", api.header, api.now),
    "sub/now": (None, "json:map", api.header, api.now),
  }

  return WebDispatcher(media, constant, dynamic, endpoints)

#==========================================================

# called by webserver (in dev mode)
# returns new webdispatcher or none if not reloaded

class WEBDIR:

  FILETIMES = { } # path = mtime

def reloadweb():

  if not WEBDIR.FILETIMES:
    # cache last modified
    _,_,filenames = next(os.walk(f"{APPSOURCE}/web"))
    for filename in filenames:
      filepath = f"{APPSOURCE}/web/{filename}"
      filetime = filemodified(filepath)
      WEBDIR.FILETIMES[filepath] = filetime

  changed = False
  for filepath,filetime in WEBDIR.FILETIMES.items():
    newtime = filemodified(filepath)
    if newtime != filetime:
      changed = True
      WEBDIR.FILETIMES[filepath] = newtime

  if not changed:
    return # done

  # reload!

  import api
  importlib.reload(api)

  import frame
  importlib.reload(frame)

  import home
  importlib.reload(home)

  return setupweb()

#==========================================================
