#!/bin/sh
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
. "${SCRIPTPATH}/superlib"
cd "${SCRIPTPATH}"

. "./unitlib"

#==========================================================

# unit * Apache 2.0 (C) 2021 librehof.com

summary()
{
  iscmd lsof # fails on git-for-windows
  islsof=${?}
  info ""
  info "Manage unit (${UNIT})"
  info ""
  info "Usage (repo):"
  info " ./${SCRIPT} vcs|origin|info"
  info " ./${SCRIPT} pointer|localpointer"
  info " ./${SCRIPT} hash|localhash|remotehash"
  info " ./${SCRIPT} update [offline]"
  info " ./${SCRIPT} fetch # cache remote"
  info ""
  info "Usage (state):"
  info " ./${SCRIPT} state # active|bound|local|bare"
  if [ "${islsof}" = 0 ]; then
  info " ./${SCRIPT} stop  # stop unit (*)"
  info " ./${SCRIPT} release # release unit+importers"
  info ""
  info "Usage (list):"
  info " ./${SCRIPT} list active|bound # importers"
  info " ./${SCRIPT} list importers"
  info " ./${SCRIPT} list imports"
  info ""
  info "(*) stopped until next setup/launch/reboot/login"
  info ""
  fi
}

#==========================================================

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit "read/write"
fi

#==========================================================

if [ "${1}" = "vcs" ] && [ "${2}" = "" ]; then

  VCS="$(getvcs)"
  exitonerror ${?}
  echo "${VCS}"
  exit 0

elif [ "${1}" = "origin" ] && [ "${2}" = "" ]; then

  getorigin
  exitonerror ${?}
  ORIGIN="${have}"
  if [ "${ORIGIN}" != "" ]; then
    echo "${ORIGIN}"
  fi
  exit 0

elif [ "${1}" = "pointer" ] && [ "${2}" = "" ]; then

  getlocalpointer
  exitonerror ${?}
  POINTER="${have}"
  getsyncstate
  exitonerror ${?}
  POINTER="${POINTER}${sign}"
  if [ "${POINTER}" != "" ]; then
    echo "${POINTER}"
  fi
  exit 0

elif [ "${1}" = "localpointer" ] && [ "${2}" = "" ]; then

  getlocalpointer
  exitonerror ${?}
  POINTER="${have}"
  if [ "${POINTER}" != "" ]; then
    echo "${POINTER}"
  fi
  exit 0

elif [ "${1}" = "info" ] && [ "${2}" = "" ]; then

  repoinfo
  exitonerror ${?}
  exit 0

elif [ "${1}" = "hash" ] && [ "${2}" = "" ]; then

  getlocalhash
  exitonerror ${?}
  HASH="${have}"
  isintact
  if [ "${?}" != 0 ]; then
    HASH="${HASH}+"
  fi
  if [ "${HASH}" != "" ]; then
    echo "${HASH}"
  fi
  exit 0

elif [ "${1}" = "localhash" ] && [ "${2}" = "" ]; then

  getlocalhash
  exitonerror ${?}
  HASH="${have}"
  if [ "${HASH}" != "" ]; then
    echo "${HASH}"
  fi
  exit 0

elif [ "${1}" = "remotehash" ] && [ "${2}" = "" ]; then

  getlocalpointer
  exitonerror ${?}
  if [ "${want}" != "" ]; then
    POINTER="${want}"
  else
    POINTER="${have}"
  fi
  if [ "${POINTER}" != "" ]; then
    HASH="$(getremotehash "${POINTER}")"
    exitonerror ${?}
    if [ "${HASH}" != "" ]; then
      echo "${HASH}"
    fi
  fi
  exit 0

fi

#==========================================================

if [ "${1}" = "state" ] && [ "${2}" = "" ]; then

  unitstate
  exitonerror ${?}
  exit 0

elif [ "${1}" = "list" ] && [ "${2}" = "bound" ]; then

  listbound
  exitonerror ${?}
  exit 0

elif [ "${1}" = "list" ] && [ "${2}" = "active" ]; then

  listactive
  exitonerror ${?}
  exit 0

elif [ "${1}" = "list" ] && [ "${2}" = "importers" ]; then

  listimporters
  exitonerror ${?}
  exit 0

elif [ "${1}" = "list" ] && [ "${2}" = "imports" ]; then

  listimports
  exitonerror ${?}
  exit 0

fi

#==========================================================

if [ -d "../log/${UNIT}" ]; then
  ensurelog "../log/${UNIT}/unit.log"
else
  ensurelog "log/unit.log"
fi

filelock "${RUNPATH}/${UNITPREFIX}.lock"
exitonerror ${?}

if [ "${1}" = "stop" ] && [ "${2}" = "" ]; then

  stopunit
  exitonerror ${?}
  exit 0

elif [ "${1}" = "release" ] && [ "${2}" = "" ]; then

  releaseunit
  exitonerror ${?}
  exit 0

elif [ "${1}" = "update" ]; then

  if [ "${2}" != "" ] && [ "${2}" != "offline" ]; then
    inputexit
  fi

  updateunit "${2}"
  exitonerror ${?}
  exit 0

elif [ "${1}" = "fetch" ]  && [ "${2}" = "" ]; then

  fetchunit
  exitonerror ${?}
  exit 0

fi

#==========================================================

inputexit
